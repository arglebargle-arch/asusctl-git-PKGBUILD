#!/bin/bash
set -euo pipefail

# https://forum.gitlab.com/t/gitlab-api-get-latest-release-version/48608
curl -s https://gitlab.com/api/v4/projects/20328305/releases/ | jq '.[]' | jq -r '.name' | head -1
