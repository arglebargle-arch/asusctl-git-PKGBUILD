
## `asusctl-git`

Added some quick hacks to allow building specific releases from the repo as `asusctl`.

Shuffled dependencies around so the package builds successfully in a clean chroot.

Build with `makepkg ... -- RELEASE=<gitref>` to build a specific release from git. Use the included `getlatest.sh`
helper to fetch a gitref for the most recent release. Use `listreleases.sh` to list all published release versions.

`makepkg ... -- RELEASE="$(bash getlatest.sh)"` will build the most recent release from the repo.

[//]: # ( vim: set tw=120: )
