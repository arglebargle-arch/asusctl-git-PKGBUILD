# Maintainer: Arglebargle <arglebargle@arglebargle.dev>
# Contributor: Static_Rocket

# shellcheck disable=SC2034,SC2154,SC2164

pkgname=asusctl-git
pkgver=4.0.6.r7.gbcf516a
pkgrel=1
pkgdesc="Asus laptop control utilities"
arch=('x86_64')
url="https://gitlab.com/asus-linux/asusctl"
license=('MPL2')
depends=('libusb' 'udev')
optdepends=(
	'ASUS-WMI-FAN-CONTROL: custom fan curve support'
	'linux-rog: deprecated name for custom fan curve capability'
	)
makedepends=('git' 'rust')
provides=('asusctl' 'asus-nb-ctrl')
conflicts=('asus-nb-ctrl-git' 'asus-nb-ctrl' 'rog-core' 'tlp')
source=('git+https://gitlab.com/asus-linux/asusctl.git')
md5sums=('SKIP')
_gitdir=${pkgname%"-git"}

# quick hack to build package from an arbitrary git ref. rather than repo HEAD
if [[ -v RELEASE ]]; then
	pkgname="${pkgname%-git}"   # reset package name to avoid aur helpers "helping" us by updating to current repo HEAD later           
	provides=('asus-nb-ctrl')   # we don't need to provide ourselves
	conflicts+=('asusctl-git')	# non -git package should conflict with -git version

	# let the user know which version we're building
	msg "build override: building '$pkgname' from git ref=$RELEASE" >&2
else
	conflicts+=('asusctl')
fi

pkgver() {
	cd "$srcdir/$_gitdir"
	git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
	# return if we're building from repo HEAD without a git ref specified
	! [[ -v RELEASE ]] &&
		return 0

	# checkout our git ref if we're building a specific version
	cd "$srcdir/$_gitdir"
	git checkout "$RELEASE"
}

build() {
	cd "$srcdir/$_gitdir"
	make build
}

package() {
	# add runtime dependencies, these aren't needed during build
	depends+=('supergfxctl' 'power-profiles-daemon>=0.9.0+14+g112df04')
	cd "$srcdir/$_gitdir"
	make DESTDIR="$pkgdir" install
}

